# Dylan's VIM config

Thanks to https://realpython.com/vim-and-python-a-match-made-in-heaven/#vim-extensions

## 1. Install Vundle

```
git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
```

## 2. Create a .vimrc file

```
git clone https://gitlab.com/DylanVanAssche/dotfiles
cp dotfiles/.vimrc ~/.vimrc
```

## 3. Installing plugins

Open VIM, run `:PluginInstall` to start Vundle.

### YouCompleteMe

```
# Install CMake, Vim and Python (Ubuntu)
sudo apt install build-essential cmake vim python3-dev nodejs npm
# Install CMake, Vim and Python (Alpine Linux)
sudo apk add python3-dev cmake vim nodejs npm clang-extra-tools

# On Alpine Linux (musl), we cannot use the bundled clangd tools:
See https://github.com/ycm-core/YouCompleteMe/issues/3875 and 
https://github.com/ycm-core/YouCompleteMe/wiki/FAQ#ycm-crashes-when-using-musl
1. rm ~/.vim/bundle/YouCompleteMe/third_party/ycmd/third_party/clang/lib/libclang*
2. Put `let g:ycm_clangd_binary_path='clangd'` in `.vimrc`.
3. DO NOT compile YCM with `--clang-completer`.


# Compile YouCompleteMe
cd ~/.vim/bundle/YouCompleteMe
python3 install.py --ts-completer
```

### Enable spellchecking for LaTeX

Add this to `~/.vim/ftplugin/tex.vim`:

```
setlocal spell spelland=en_gb
```
